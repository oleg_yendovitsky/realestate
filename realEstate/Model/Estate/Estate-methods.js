

model.Estate.methods.addMainPhoto = function(fileName, ext) {
	var uuid = generateUUID();
	var newFileName = "image_"+uuid+"."+ext;
	var path = ds.getModelFolder().path;
	var thisFile = new File(path+'DataFolder/tmp/'+fileName);
	var newPath = path+"DataFolder/images/"+newFileName;
	if(thisFile){
		thisFile.moveTo(newPath);
		var p = new ds.Picture();
		p.Name = "image_"+uuid;
		p.Photo = newPath;
		p.save();
	}
	return p.ID;
};
model.Estate.methods.addMainPhoto.scope = "public";

model.Estate.methods.addPhotos = function(arr) {
	var newArrOfID = [];
	arr.forEach(function(item){
		var uuid = generateUUID();
		var newFileName = "image_"+uuid+"."+item[1];
		var path = ds.getModelFolder().path;
		var thisFile = new File(path+'DataFolder/tmp/'+item[0]);
		var newPath = path+"DataFolder/images/"+newFileName;
		if(thisFile){
			thisFile.moveTo(newPath);
			var p = new ds.Picture();
			p.Name = "image_"+uuid;
			p.Photo = newPath;
			p.save();
			newArrOfID.push(p.ID);
		}
	});
	return newArrOfID;
};
model.Estate.methods.addPhotos.scope = "public";