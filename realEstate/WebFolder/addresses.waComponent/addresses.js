﻿
(function Component (id) {// @lock

// Add the code that needs to be shared between components here

function constructor (id) {

	// @region beginComponentDeclaration// @startlock
	var $comp = this;
	this.name = 'addresses';
	// @endregion// @endlock

	this.load = function (data) {// @lock

	// @region namespaceDeclaration// @startlock
	var container4 = {};	// @container
	// @endregion// @endlock

	// eventHandlers// @lock

	container4.click = function container4_click (event)// @startlock
	{// @endlock
		waf.widgets.component2.loadComponent({
			path:'/estates.waComponent',
			userData:{addressID:waf.sources.component1_address1.ID},
			onSuccess:function(event){
				waf.widgets.tabView1.selectTab(2);	
			}	
		})
	};// @lock

	// @region eventManager// @startlock
	WAF.addListener(this.id + "_container4", "click", container4.click, "WAF");
	// @endregion// @endlock

	};// @lock


}// @startlock
return constructor;
})();// @endlock
