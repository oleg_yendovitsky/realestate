﻿
(function Component (id) {// @lock

// Add the code that needs to be shared between components here

function constructor (id) {

	// @region beginComponentDeclaration// @startlock
	var $comp = this;
	this.name = 'estateDetail';
	// @endregion// @endlock

	this.load = function (data) {// @lock

	// @region namespaceDeclaration// @startlock
	var fileUpload4 = {};	// @fileUpload
	var fileUpload3 = {};	// @fileUpload
	// @endregion// @endlock

	// eventHandlers// @lock
	var thisEstate;
	if(data.userData.estateID){
			console.log(data.userData.estateID);
			$comp.sources.estate1.query("ID == :1", data.userData.estateID, {
				onSuccess:function(event){
					thisEstate = event.dataSource;
					waf.widgets.button7.show();
					$('.se-pre-con').fadeOut('fast');
				}
			});
	}else{
		console.log("data.userData.estateID is undefined");
	}

	
	fileUpload4.filesUploaded = function fileUpload4_filesUploaded (event)// @startlock
	{// @endlock
		var file = waf.widgets.component3_fileUpload4.getFiles();
		setTimeout(function(){
			var arr = file[0].name.split('.');
			var fileName = file[0].name;
			var ext = arr[1];
			console.log(file);
			$comp.sources.estate1.callMethod({
				method:"addMainPhoto",
				onSuccess:function(event){
					console.log(event.result);
					var pictureID = event.result;
					$comp.sources.picture.query("ID == :1", pictureID,{
						onSuccess:function(event){
							thisEstate.MainPhoto.set(event.dataSource);
							$comp.sources.estate1.save();
						}
					});
					$comp.sources.estate1.serverRefresh();
				}
			},fileName,ext)
		},200)
	};// @lock

	fileUpload3.filesUploaded = function fileUpload3_filesUploaded (event)// @startlock
	{// @endlock
		var files = waf.widgets.component3_fileUpload3.getFiles();
		setTimeout(function(){
			console.log(files);
			var fileArr = [];
			files.forEach(function(item){
				var arr = item.name.split('.');
				var fileName = item.name;
				var ext = arr[1];
				fileArr.push([fileName, ext]);
			});
			console.log(fileArr);
			$comp.sources.estate1.callMethod({
				method:"addPhotos",
				onSuccess:function(event){
					var result = event.result;
					result.forEach(function(item){
						$comp.sources.picture.query("ID == :1", item, {
							onSuccess:function(event){
								var picture = event.dataSource;
								picture.Estate_ID.set(thisEstate);
								picture.save();
							}
						});
					});
					setTimeout(function(){$comp.sources.estate1.serverRefresh()},300);
				}
			},fileArr);
		},200);
	};// @lock

	// @region eventManager// @startlock
	WAF.addListener(this.id + "_fileUpload4", "filesUploaded", fileUpload4.filesUploaded, "WAF");
	WAF.addListener(this.id + "_fileUpload3", "filesUploaded", fileUpload3.filesUploaded, "WAF");
	// @endregion// @endlock

	};// @lock


}// @startlock
return constructor;
})();// @endlock
