﻿
WAF.onAfterInit = function onAfterInit() {// @lock

// @region namespaceDeclaration// @startlock
	var button7 = {};	// @button
	var documentEvent = {};	// @document
	var objectType1Event = {};	// @dataSource
// @endregion// @endlock

// eventHandlers// @lock

	button7.click = function button7_click (event)// @startlock
	{// @endlock
		var tab = waf.widgets.tabView1.getSelectedTab().index;
		switch(tab){
			case 2:{
					waf.widgets.tabView1.selectTab(1)
					waf.widgets.button7.hide();	
				};break;
			case 3:waf.widgets.tabView1.selectTab(2);break;
		}
	};// @lock

	$('#menuItem9').css('width','90px');
	$('#menuItem8').css('width','90px');
	$('#menuItem7').css('width','90px');
	

	documentEvent.onLoad = function documentEvent_onLoad (event)// @startlock
	{// @endlock

	};// @lock
	

	$('#dataGrid1').css('border-radius','0px');

	objectType1Event.onCurrentElementChange = function objectType1Event_onCurrentElementChange (event)// @startlock
	{// @endlock
		waf.sources.estate.query("Type_ID.ID == :1",event.dataSource.ID);
	};// @lock

// @region eventManager// @startlock
	WAF.addListener("button7", "click", button7.click, "WAF");
	WAF.addListener("document", "onLoad", documentEvent.onLoad, "WAF");
	WAF.addListener("objectType1", "onCurrentElementChange", objectType1Event.onCurrentElementChange, "WAF");
// @endregion
};// @endlock
