﻿
(function Component (id) {// @lock

// Add the code that needs to be shared between components here

function constructor (id) {

	// @region beginComponentDeclaration// @startlock
	var $comp = this;
	this.name = 'estates';
	// @endregion// @endlock

	this.load = function (data) {// @lock

	// @region namespaceDeclaration// @startlock
	var objectType1Event = {};	// @dataSource
	var dataGrid1 = {};	// @dataGrid
	// @endregion// @endlock

	// eventHandlers// @lock

	objectType1Event.onCurrentElementChange = function objectType1Event_onCurrentElementChange (event)// @startlock
	{// @endlock
		
	};// @lock

	if(data.userData.addressID){
		console.log(data.userData.addressID);
		$comp.sources.estate1.query("Adddress_ID.ID == :1",data.userData.addressID,{
			onSuccess:function(event){
				console.log(event);
				waf.widgets.button7.show();
				$('.se-pre-con').fadeOut('fast');
			}
		});
	}else{
		console.log('data.userData.addressID is undefined');
	}

	dataGrid1.onRowDblClick = function dataGrid1_onRowDblClick (event)// @startlock
	{// @endlock
		waf.widgets.component3.loadComponent({
			path:'/estateDetail.waComponent',
			userData:{estateID:waf.sources.component2_estate1.ID},
			onSuccess:function(event){
				waf.widgets.tabView1.selectTab(3);	
			}	
		})
	};// @lock

	// @region eventManager// @startlock
	WAF.addListener(this.id + "_objectType1", "onCurrentElementChange", objectType1Event.onCurrentElementChange, "WAF");
	WAF.addListener(this.id + "_dataGrid1", "onRowDblClick", dataGrid1.onRowDblClick, "WAF");
	// @endregion// @endlock

	};// @lock


}// @startlock
return constructor;
})();// @endlock
